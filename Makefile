html:
	Rscript -e 'rmarkdown::render("index.Rmd")'

run:
	Rscript -e 'rmarkdown::run(shiny_args=list(port=5000))'

