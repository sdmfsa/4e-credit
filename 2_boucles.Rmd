---
title: Boucles
author: "Carlos C.S. <cachs1@ulaval.ca>"
date: "`r format(Sys.time(), '%d %B, %Y')`"
---

## Structures de contrôle
<small> <a href='./www/2/slides.html' target = "_blank" title='Diapos'><i class='fa fa-chalkboard'></i></a> Diapos | <a href="https://cran.r-project.org/doc/contrib/Goulet_introduction_programmation_R.pdf" target="_blank"><i class="fa fa-book-open"></i></a> Chap. 2 et 3</small>

Plusieurs structures de contrôle sont disponibles sur R. Ces structures partagent leur logique et nom avec plusieurs autres langages de programmation et leur but est de *contrôler* l'exécution du code. Nous allons faire deux exemples avec :

+ Structure `if` pour une exécution conditionnelle.
+ Structure `for` pour créer une **boucle**.

### Exécution conditionnelle

+ Comparez les valeurs se trouvant dans les variables `x` et `y` ci-bas.
+ Affichez sur l'écran un message qui indique si `x` est plus grand que `y`.


```{r if, exercise=TRUE}
x <- 1100
y <- 100
```

```{r if-solution}
x <- 1100
y <- 100

if(x > y) {
  print("x et plus grand que y")
} else {
  print("y et plus grand que x")
}
```

### Boucle

+ Parcourez le vecteur `input` ci-bas à l'aide d'une boucle `for`.
+ Affichez sur l'écran chaque élément du vecteur `input`.

```{r for, exercise=TRUE}
input <- c("BB.TO", "AC.TO", "XIU.TO", "XGD.TO")
```

```{r for-solution}
input <- c("BB.TO", "AC.TO", "XIU.TO", "XGD.TO")

for (ticker in input) {
  texte <- paste("Ticker :", ticker)
  print(texte)
}
```

###

Nous allons maintenant trouver des prix historiques sur la base de données CRSP. Pour parcourir les éléments du vecteur ou de la matrice, nous allons utiliser une **boucle**.

### Données CRSP

Suivez ces instructions pour obtenir un fichier `.csv` avec le niveau et les rendements de l'indice S&P 500 à l'aide de CRSP via wrds <a href="https://wrds-www.wharton.upenn.edu" target="_blank"><i class="fas fa-external-link-square-alt"></i></a> .


```{r, include=FALSE}
knitr::knit("2-boucles/crsp.Rmd", "2-boucles/crsp.md", quiet = TRUE)
```

```{r, echo=FALSE}
tabsetPanel(type = "tabs",
            tabPanel("Instructions", shiny::includeMarkdown("2-boucles/crsp.md")),
            tabPanel("WRDS", shiny::img(src="images/wrds-crsp.gif", width="80%"))
)
```

### Importer les données CRSP

Utilisez la commande suivante pour lire les données sur `R` :

```{r , eval=FALSE}
crsp <- read.csv("wrds1925.csv", header=TRUE, sep=",")
crsp <- na.omit(crsp)  # Ignorer les NA
```

Où vous devez remplacer `wrds1925.csv` par le nom de votre fichier `.csv`.

Ensuite, sauvegardez les 3 pires rendements de l'indice dans un `data.frame`. Le tableau résultant ne doit contenir que deux colonnes et 3 lignes, soit les pires 3 rendements ainsi que les dates correspondantes en ordre chronologique.

```{r prepare-crsp}
ROWS0 <- 1105
crsp <- data.frame(caldt=as.Date(seq(from=1, by=30, length.out=ROWS0),
                                 origin="1926-01-30"),
                   spindx=cumsum(c(120, histrates <- rnorm(ROWS0-1)))
)
crsp$sprtrn <- c(0.02, crsp$spindx[-1]/crsp$spindx[-ROWS0]-1)
crsp$caldt <- as.integer(gsub("-", "", crsp$caldt))

set.seed(123456)
CF <- rnorm(10, mean=100, sd=10)  # Flux monétaires
```

```{r crsp, exercise=TRUE, exercise.setup='prepare-crsp'}
head(crsp)
```

```{r crsp-solution}
head(crsp)  # Premières lignes du tableau

crsp_sorted <- crsp[order(crsp$sprtrn), ]
crsp_top3 <- crsp_sorted[1:3, ]

(crsp_top3 <- crsp_top3[order(crsp_top3$caldt), ])  # Pas encore de boucles
```

> Les données que vous voyez ici ont été simulées.


### Boucles

> Cet exemple est une version modifiée de l'exemple 3.9 du livre [Introduction à la programmation en R](https://cran.r-project.org/doc/contrib/Goulet_introduction_programmation_R.pdf) de [Vincent Goulet](https://gitlab.com/vigou3) publié sous une licence [CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/deed.fr).

Trouvons les niveaux "record" de l'indice, c'est-à-dire la première fois que un niveau donné a été observé.

<div id="filter-hint">
**Indice:** Les structures de contrôle `for`, `repeat` ou `while` peuvent vous aider à parcourir le vecteur.
</div>

```{r records, exercise=TRUE, exercise.setup='prepare-crsp'}
dim(crsp)
```

```{r records-solution}
recordsdf <- crsp[1, ]  # 1ère observation

for(i in 2:nrow(crsp))  {
  if(crsp$spindx[i] > max(recordsdf$spindx))  {  # Record trouvé
    recordsdf <- rbind(recordsdf, crsp[i, ])
  }
}

dim(recordsdf)

```

> Les données que vous voyez ici ont été simulées.

###

Pour conclure cette section, faisons deux exemples classiques de finance : calcul de VAN et calcul de TRI. Pour le TRI, nous ferons appel à une boucle.

### Exemple avec la VAN

En supposant que le premier flux monétaire d'un actif financier sera reçu au temps $t=1$ et que nous sommes au temps $t=0$, la valeur présente de cet actif peut être calculée comme:

$$
\text{NPV}_0 = \sum_{t=1}^T \frac{\text{CF}_t}{(1+k)^t}
$$

Pour calculer la valeur actuelle d'un actif procurant les flux monétaires, nous commençons par sauvegarder ces chiffres dans une variable nommée `CF`, où le premier élément du vecteur correspond au temps $t=1$, le deuxième au temps $t=2$ et ainsi de suite. Nous allons utiliser un taux d'intérêt $k=0.05$.

```{r van, exercise=TRUE, exercise.setup='prepare-crsp'}
CF
```

```{r van-hint-1}
nbT <- length(CF)
k <- 0.05
```

```{r van-hint-2}
nbT <- length(CF)
k <- 0.05

facteurs <- 1/((1+k)^(1:nbT))
```

```{r van-hint-3}
nbT <- length(CF)
k <- 0.05

facteurs <- 1/((1+k)^(1:nbT))
(NPV <- sum(CF*facteurs))
```

### TRI

À l'aide des données de la question précédente, quel est le taux de rendement interne (TRI ou *IRR*) nécessaire pour avoir une valeur actuelle égale à 900 ?

<div id="filter-hint">
**Indice** : utilisez les structures de contrôle disponibles (`for`, `while` ou `repeat`).
</div>

```{r tri, exercise=TRUE, exercise.setup='prepare-crsp'}
goal  <-  900
nbT <- length(CF)
```

```{r tri-hint-1}
goal  <-  900
nbT <- length(CF)

k <- 0  # Point de départ
NPV <- 0  # Point de départ
```

```{r tri-hint-2}
goal  <-  900
nbT <- length(CF)

k <- 0  # Point de départ
NPV <- 0  # Point de départ

while(abs(NPV-goal) > 1) {  # On cherche NPV==goal
  facteurs <- 1/((1+k)^(1:nbT))
  NPV <- sum(CF*facteurs)

  k <- k + 1E-4
}
```

```{r tri-hint-3}
goal  <-  900
nbT <- length(CF)

k <- 0  # Point de départ
NPV <- 0  # Point de départ

while(abs(NPV-goal) > 1) {  # On cherche NPV==goal
  facteurs <- 1/((1+k)^(1:nbT))
  NPV <- sum(CF*facteurs)

  k <- k + 1E-4
}

cat("Premier taux trouvé : ", k)

facteurs <- 1/((1+k)^(1:nbT))
(NPV <- sum(CF*facteurs))
```
