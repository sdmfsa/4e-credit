---
title: "Boucles"
subtitle: "GSF-6029 | 4e Crédit"
author: "Carlos Chaparro  [<i class='fas fa-envelope'></i>](mailto:carlos.chaparro@fsa.ulaval.ca)"
date: "`r format(Sys.time(), '%Y-%m')`"
output:
  xaringan::moon_reader:
    lib_dir: public/libs
    css: [default, metropolis, metropolis-fonts, ../css/extra.css]
    nature:
      highlightStyle: github
      countIncrementalSlides: false
      highlightLines: true

---

```{r, setup, include=FALSE}
htmltools::tagList(rmarkdown::html_dependency_font_awesome())
```

# Bases

## Listes

```{r,eval=F}
element1 <- rnorm(10)
element2 <- "10 tirages d'une loi normale centrée reduite"
*maListe <- list(tirages = element1, texte = element2)

print(maListe)
print(maListe$tirages)
```

---

## Vecteurs
```{r,eval=F}
vecteur1 <- c(1,3,5,7,9)  # Créer un vecteur manuellement
```

```{r,eval=F}
(tirages <- maListe$tirages)  # Attention aux paranthèses
is.vector(tirages)
```

---

## Indexation

```{r,eval=F}
tirages[1]

pos <- 1

*tirages[pos]  # tirages[pos=1] = tirages[1]
```

---

## Indexation
<small>(cont.)</small>

```{r,eval=F}
(pos <- c(1,3,5,7,9))

tirages[pos]
```

---

## Indexation
<small>(cont.)</small>

```{r,eval=F}
tirages
tirages[1:5]
(pos <- c(TRUE,TRUE,TRUE,TRUE,TRUE,FALSE,FALSE,FALSE,FALSE,FALSE))
*tirages[pos]
```

---


## Indexation
<small>(cont.)</small>

```{r,eval=F}
N <- 1e4
tirages <- rnorm(N)
pos <- (tirages<1.64)

head(tirages[pos])
length(tirages[pos])
```

---

## Indexation
<small>(cont.)</small>

```{r,eval=F}
num <- length(tirages[pos])

num/N
*pnorm(1.64)
```

---

## Matrices
```{r,eval=F}
matrice1 <- matrix(c(1,2,3,4,5,6), nrow = 3, ncol = 2)
print(matrice1)

matrice2 <- matrix(c(1,2,3,4,5,6), nrow = 3, ncol = 2, byrow = TRUE)
print(matrice2)
```

---

## Indexation

```{r,eval=F}
matrice1
matrice1[1, ]  # Ligne 1, toutes les colonnes
matrice1[, 1]  # Colonne 1, toutes les lignes

```

---

# Opérateurs

## Arithmétique
<small>Exemples tirés du livre du livre [Introduction à la programmation en R](https://cran.r-project.org/doc/contrib/Goulet_introduction_programmation_R.pdf) de [Vincent Goulet](https://gitlab.com/vigou3) publié sous une licence [CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/deed.fr)</small>

```{r,eval=F}
5 * c(2, 3, 8, 10)         # multiplication par une constante
c(2, 6, 8) + c(1, 4, 9)    # addition de deux vecteurs
c(0, 3, -1, 4)^2           # élévation à une puissance
```

---

## Recyclage

```{r,eval=F}
8 + 1:10                   # 8 est recyclé 10 fois
c(-2, 3, -1, 4)^(1:4)      # quatre puissances différentes
c(-2, 3, -1, 4)^(1:2)      # en recyclant le vecteur c(1,2)
```

---

## Algèbre linéaire
```{r,eval=F}
poids <- c(0.3,0.5,0.20)
mat_rendements <- matrix(c(rnorm(250),rt(250,6), rt(250,5)), ncol=3)

*(vec_rendements <- colMeans(mat_rendements))
*(varcovar <- cov(mat_rendements))
```

---

## Algèbre linéaire
<small>(cont.)</small>
$$
\text{E}(R_p) = \omega^{T} \cdot R
$$

```{r,eval=F}
(r_p <- t(poids)%*%vec_rendements)
```

---

## Algèbre linéaire
<small>(cont.)</small>
$$
\text{Var}(R_p) = \omega^{T} \cdot \Omega \cdot  \omega
$$

```{r,eval=F}
(sigma_p <- t(poids)%*%varcovar%*%poids)
```

---

## Modulo

```{r,eval=F}
5 %% 2                     # 5/2 = 2 reste 1

for (i in 1:50) {
  # Affiche la valeur du compteur toutes les 10 itérations.
*  if (0 == i %% 10)
    print(i)
}
```

---

## ET, OU, !

```{r,eval=F}
(1 < 2)                 # Vrai ou Faux?
(pi < 3.14)             # pi = 3.141593
(1 < 2) && (pi < 3.14)  # ET
(1 < 2) || (pi < 3.14)  # OU
```

---

## ET, OU, !
<small>(cont.)</small>

```{r,eval=F}
(a <- c(!(1 < 2), !(pi < 3.14)))  # ! = Négation logique
(b <- c(TRUE,!FALSE))
a & b                            # Comparer chaque élément
a && b                           # Comparer le tout
```

---

## ==

```{r,eval=F}
x <- 3  # Équivalent : x = 3

x == 3
x == 4

(y <- (x == 3))
(y + 1)  # TRUE vaut 1 et FALSE vaut 0.
```

---

## Appel de fonctions

```{r,eval=F}
seq(from = 1, to = 10)        # Équivalent : 1:10
seq(from = 1, to = 10, by=2)  # Voir ?seq au besoin
rep(1, 10)                    # Utilisation de base, voir ?rep
```

---

## data.frame

```{r,eval=F}
v1 <- c(1,2,5,3)
v2 <- c("a","b","a","b")
(tableau <- as.data.frame(cbind(v1,v2)))
index <- tableau$v2=="a"
tableau[index, ]
```

---

## which

```{r,eval=F}
(x <- sample(1:200, 50))

index <- which(x>30 & x<100)  # Quelles valeurs sont > 30 ET < 100
x[index]
*#?subset
```

---

## outer

```{r, eval=F}
x <- c(1, 2, 4, 7, 10, 12)
y <- c(2, 3, 6, 7, 9, 11)
outer(x, y)                # produit extérieur
x %o% y                    # équivalent plus court
```

---

# Boucles

## for

```{r,eval=F}

for (i in 1:10) {
*  if (i %% 2) {
    print("---")
  } else {
    print("Bingo!")
  }
}

```

---

## while

```{r,eval=F}

i <- 1

while(i <= 10) {
  if (i %% 2) {
    print("---")
  } else {
    print("Bingo!")
  }
  i <- i + 1
}
```

---

## repeat

```{r,eval=F}
i <- 1

repeat {
  if (i %% 2) {
    # print("---")
  } else {
    print("Bingo!")
  }

  if (i==10) break

  i <- i + 1
}
```

---

## Dates

```{r,eval=F}
(dates <- c(19251231, 19261231))
(dates <- as.Date(as.character(dates), format="%Y%m%d"))

```

<a href="https://statistics.berkeley.edu/computing/r-dates-times" target="_blank"><i class="fas fa-question-circle"></i></a> Les dates

---

## Graphique

```{r, eval=F, fig.height = 3, fig.width = 6}
set.seed(123465)
data <- rnorm(31,mean=-10,sd=1)
plot(data, type="l", axes=FALSE, main="Titre", xlab="X", ylab="Y")
abline(h=min(data), col=2)
abline(h=max(data), col=3)

axis(1,at=1:31)
axis(2,at=round(min(data)):round(max(data)))
```
