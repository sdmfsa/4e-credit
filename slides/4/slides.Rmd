---
title: "Frontière efficiente"
subtitle: "GSF-6016 | 4e Crédit"
author: "Carlos Chaparro  [<i class='fas fa-envelope'></i>](mailto:carlos.chaparro@fsa.ulaval.ca)"
date: "`r format(Sys.time(), '%Y-%m')`"
output:
  xaringan::moon_reader:
    lib_dir: public/libs
    css: [default, metropolis, metropolis-fonts, ../css/extra.css]
    nature:
      highlightStyle: github
      countIncrementalSlides: false
      highlightLines: true
---

# fPortfolio

## Les données

```{r, message=FALSE}
library(fPortfolio)

myData <- read.csv("returns.csv")
myData <- as.timeSeries(myData[, 1:5])
```

.pull-left[
```{r}
head(myData, 3)
```
]

.right[
.pull-right[
```{r}
dim(myData)
class(myData)
```
]
]

---

## Positions longues
<small>Frontière avec contraintes, exemples tirés de [Rmetrics (ch. 18)](https://www.rmetrics.org/ebooks-portfolio)</small>

```{r, eval=TRUE}
mySpec <- portfolioSpec()
setNFrontierPoints(mySpec) <- 20
longFrontier <- portfolioFrontier(myData, mySpec)

head(getWeights(longFrontier)) # Liste 11.10 de la documentation
# ?getPortfolio
```

---

# Graphiques

## Frontière
<small>`frontierPlot`</small>

```{r, eval = FALSE}
frontierPlot(longFrontier)
```

## Personnalisé
<small>`tailoredFrontierPlot`</small>

```{r, eval=FALSE}
tailoredFrontierPlot(object = longFrontier,
                     mText = "MV Portfolio - LongOnly Constraints",
                     risk = "Cov")
```

## Autres

```{r, eval=FALSE, fig.height=4, fig.width=6}
weightsPlot(longFrontier)
weightedReturnsPlot(longFrontier)
covRiskBudgetsPlot(longFrontier)
```
---

# Contraintes

## Par titre
<small>`boxConstraints`</small>

```{r, eval=FALSE}
boxConstraints <- c("minW[1:20]=-0.10", "maxW[1:20]=0.20")
boxFrontier <- portfolioFrontier(data = myData, spec = mySpec,
                                 constraints = boxConstraints)
```

> Il suffit de rentrer des poids négatifs pour autoriser les ventes à découvert.

---

## Par secteur
<small>`groupConstraints`</small>

```{r, eval=FALSE}
groupConstraints <- c("minsumW[1:4]=0.05",
                      "maxsumW[c(13,14,17,18)]=0.15")
groupFrontier <- portfolioFrontier(data = myData, spec = mySpec,
                                   constraints = groupConstraints)
```

> Il s'agit des pourcentages min/max (entre `0` et `1`) qui seront investis dans un groupe de titres.

---

## Contraintes par Titre et Secteur
<small>`boxgroupConstraints`</small>

```{r, eval=FALSE}
boxgroupConstraints <- c(boxConstraints, groupConstraints)

boxgroupFrontier <- portfolioFrontier(data = myData, spec = mySpec,
                                      constraints = boxgroupConstraints)
```

---

## Ventes à découvert illimitées
<small>Frontière analytique avec `solveRshortExact`</small>

```{r, eval=FALSE}
shortSpec <- portfolioSpec()
setNFrontierPoints(shortSpec) <- 20
setSolver(shortSpec) <- "solveRshortExact"

shortFrontier <- portfolioFrontier(data = myData, spec = shortSpec,
                                   constraints = "Short")
shortFrontierPts <- frontierPoints(object = shortFrontier)
```

```{r, eval=FALSE, fig.height=5}
tailoredFrontierPlot(object = boxgroupFrontier,
                     mText = "MV Portfolio - Box/Group Constraints",
                     risk = "Cov", sharpeRatio = FALSE)
lines(shortFrontierPts, col = "red", lwd = 2, lty = 2)
```
