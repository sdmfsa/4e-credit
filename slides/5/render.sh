#!/usr/bin/env

Rscript -e 'rmarkdown::render("slides.Rmd", output_dir="public")'
rm -rf ../../www/${PWD##*/}
mv public ../../www/${PWD##*/}
cp ../img/* ../../www/img/
cp ../css/*.css ../../www/css/

