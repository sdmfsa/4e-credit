---
title: "Régression linéaire"
subtitle: "GSF-6029 | 4e Crédit"
author: "Carlos Chaparro  [<i class='fas fa-envelope'></i>](mailto:carlos.chaparro@fsa.ulaval.ca)"
date: "`r format(Sys.time(), '%Y-%m')`"
output:
  xaringan::moon_reader:
    lib_dir: public/libs
    css: [default, metropolis, metropolis-fonts, ../css/extra.css]
    nature:
      highlightStyle: github
      countIncrementalSlides: false
      highlightLines: true

---

```{r, setup, include=FALSE}
htmltools::tagList(rmarkdown::html_dependency_font_awesome())
```

# Régression

## Préparer les données
<small>Données disponibles sur le site de Kenneth R. French <a href="http://mba.tuck.dartmouth.edu/pages/faculty/ken.french/data_library.html#Research" target="_blank"><i class="fas fa-external-link-square-alt"></i></a></small>

+ Tableaux : *Fama/French 3 Factors* et *25 Portfolios Formed on Size and Book-to-Market (5 x 5)*.
+ « *Like families, tidy datasets are all alike but every messy dataset is messy in its own way* »,  Hadley Wickham <a href="https://tidyr.tidyverse.org/articles/tidy-data.html" target="_blank"><i class="fas fa-external-link-square-alt"></i></a>


```{r, include=FALSE}
## Set up regression.Rmd

# X variables

adresse_url <- "http://mba.tuck.dartmouth.edu/pages/faculty/ken.french/ftp/F-F_Research_Data_Factors_CSV.zip"
temp <- tempfile()
download.file(adresse_url, temp)
facteurs_ff <- read.csv(unz(temp, "F-F_Research_Data_Factors.CSV"), skip=3)
unlink(temp)

facteurs_ff <- facteurs_ff[-(1131:nrow(facteurs_ff)), ]
facteurs_ff <- apply(facteurs_ff, 2, as.numeric)

# Y variables

adresse_url <- "http://mba.tuck.dartmouth.edu/pages/faculty/ken.french/ftp/25_Portfolios_5x5_CSV.zip"
temp <- tempfile()
download.file(adresse_url, temp)
ports_ff <- read.csv(unz(temp, "25_Portfolios_5x5.CSV"), skip=15)
unlink(temp)

ports_ff <- ports_ff[-(1131:nrow(ports_ff)), ]
ports_ff <- apply(ports_ff, 2, as.numeric)

## Préparer les variables
pos <- which(ports_ff[, "X"] <= 199112 & ports_ff[, "X"] >= 196307)
ff_1993 <- ports_ff[pos, ]

pos <- which(facteurs_ff[, "X"] <= 199112 & facteurs_ff[, "X"] >= 196307)
ff_rf <- facteurs_ff[pos, "RF"]

ff_1993_y <- ff_1993[, -1] - ff_rf  # Les Y
ff_1993_x <- facteurs_ff[pos, -1]   # Les X

```

---

## lm
<small>Tableau 4 du papier de Fama et French (1993) <a href="http://mba.tuck.dartmouth.edu/pages/faculty/ken.french/data_library.html#Research" target="_blank"><i class="fas fa-external-link-square-alt"></i></a>
</small>

```{r}
## Portefeuille 1 vs Mkt.RF

lm(ff_1993_y[, 1] ~ ff_1993_x[, 1])

```

---

## Algèbre linéaire
<small>Moindres carrés ordinaires <a href="https://en.wikipedia.org/wiki/Linear_regression#Least-squares_estimation_and_related_techniques" target="_blank"><i class="fas fa-external-link-square-alt"></i></a></small>


$$
\hat{\beta} = \left( X^{T} X\right)^{-1} X^{T} Y
$$

.pull-left[
```{r}
## Portefeuille 1 vs Mkt.RF
x <- cbind(1, ff_1993_x[, 1])
y <- ff_1993_y[, 1]

solve(t(x)%*%x)%*%t(x)%*%y
```
]

.pull-right[
```{r}
out <- lm(y ~ ff_1993_x[, 1])
out$coefficients
```
]

---

# R Markdown

## Rapports dynamiques

+ Produire des fichiers PDF, Word, HTML, etc.
+ Éviter LaTeX.
+ Tout à partir de `R/RStudio`.
+ R Markdown: The Definitive Guide <a href="https://bookdown.org/yihui/rmarkdown/" target="_blank"><i class="fas fa-external-link-square-alt"></i></a>

---

# RIT

## ALGO 1
<small>*Algorithmic Trading*</small>

+ Instructions d'installation <a href="http://sdmapps.ca/latrousse/fr/rit/install/" target="_blank"><i class="fas fa-external-link-square-alt"></i></a>

+ Documentation :
  + RIT VBA API Tutorial <a href="http://ritc.rotman.utoronto.ca/documents/2017/RIT%20-%20User%20Guide%20-%20VBA%20API%20Documentation.pdf" target="_blank"><i class="fas fa-external-link-square-alt"></i></a>
  + RIT REST API Tutorial <a href="http://ritc.rotman.utoronto.ca/documents/2020/RIT%20-%20User%20Guide%20-%20REST%20API%20Documentation.pdf" target="_blank"><i class="fas fa-external-link-square-alt"></i></a>

```{r, eval=FALSE}
while(time_remaining > 5 && time_remaining < 295)  {

*   if(ask_a < bid_b) {
        buy_a()
        sell_b()
    }

    Sys.sleep(1)
}
```

---

## Exercice

+ Recréer le fichier Excel qui se trouve à la page 24 de la documentation VBA <a href="http://ritc.rotman.utoronto.ca/documents/2017/RIT%20-%20User%20Guide%20-%20VBA%20API%20Documentation.pdf" target="_blank"><i class="fas fa-external-link-square-alt"></i></a>

