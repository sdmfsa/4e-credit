---
title: "Activité 8"
subtitle: "<small>GSF-6022</small>"
author: "<small><i class='fas fa-portrait'></i> carlos.chaparro<span>@</span>fsa.ulaval.ca</small>"
date: "<span style='color:gray'><small><i class='far fa-clock'></i> `r format(Sys.time(), '%Y-%m')`</small></span>"
output:
  revealjs::revealjs_presentation:
    transition: fade
    background_transition: fade
    self_contained: false
    reveal_plugins: ["zoom", "notes", "chalkboard", "menu"]
    css: ../css/styles.css
    theme: black
    center: true
    reveal_options:
      chalkboard:
        theme: whiteboard
        toggleNotesButton: true
      menu:
        numbers: false
      previewLinks: false
      slideNumber: true
---

#  Simulations

## Variables i.i.d.
<small>Indépendantes et identiquement distribuées <a href="https://fr.wikipedia.org/wiki/Variables_ind%C3%A9pendantes_et_identiquement_distribu%C3%A9es" target="_blank"><i class="fas fa-external-link-square-alt"></i></a></small>

$$
\epsilon_i \sim N(0,1)
$$

```{r}
nb_obs <- 100
epsilons_iid <- rnorm(nb_obs, 0, 1)
```

<center>
```{r, echo=FALSE, message=FALSE, warning=FALSE}
library(plotly)
mydata <- data.frame(Col1=1:nb_obs, Col2=epsilons_iid)

x <- list(title = "t")
y <- list(title = "ϵ")
p <- plot_ly(data=mydata, x = ~Col1, y = ~Col2, type='scatter',
             name = "X_t") %>%
  layout(title = '1000 réalisations i.i.d.',
         paper_bgcolor = '#222',
         plot_bgcolor = '#222',
         showlegend = FALSE,
         xaxis = x, yaxis = y,
         height = 300, width = 400) %>%
  config(displayModeBar = F)
p
```
</center>

## i.i.d. ?

$$
\epsilon_t \sim N(\epsilon_{t-1}, 1)
$$

```{r}
set.seed(1)

nb_steps <- 100
epsilons <- numeric(nb_steps)
epsilons[1] <- 0

for (t in 2:nb_steps) {
  epsilons[t] <- epsilons[t-1] + rnorm(1)
}
```


## Graphique
<small>$\epsilon_t \sim N(\epsilon_{t-1}, 1)$</small>

<center>
```{r, echo=FALSE, message=FALSE, warning=FALSE}
mydata <- data.frame(Col1=1:nb_steps, Col2=epsilons)

x <- list(title = "t")
y <- list(title = "ϵ")
p <- plot_ly(data=mydata, x = ~Col1, y = ~Col2, mode = 'lines', type='scatter',
             name = "X_t", color = "orange") %>%
  layout(width = 0.5,
         paper_bgcolor = '#222',
         plot_bgcolor = '#222',
         showlegend = FALSE,
         xaxis = x, yaxis = y) %>%
  config(displayModeBar = F, mathjax="cdn")
p
```
</center>

## Autocorrélation {#acf}

```{r}
acf(epsilons_iid, plot = FALSE, lag.max = 5)
acf(epsilons, plot = FALSE, lag.max = 5)
```

# Monte-Carlo

## Khi carré {#khi-deux}
<small>Loi du $\chi^2$ <a href="https://fr.wikipedia.org/wiki/Loi_du_%CF%87%C2%B2#D%C3%A9finition_et_caract%C3%A9ristiques" target="_blank"><i class="fas fa-external-link-square-alt"></i></a></small>

```{r}
set.seed(1)

k <- 10
petit_x <- (rnorm(k)^2)
grand_x <- sum(petit_x)  # Un tirage

nsim <- 1e5
chi_data <- numeric(nsim)  # Un échantillon

for (i in 1:nsim) {
  petit_x <- (rnorm(k)^2)
  grand_x <- sum(petit_x)
  chi_data[i] <- grand_x
}
```

## Probabilités

```{r}
index  <- (chi_data > 10)
num    <- sum(index)

(proba <- num/nsim)  # Par Monte-Carlo

(1 - pchisq(10,10))  # Vérification
```

# Bases de données

## Connexion
<small>Exemple avec DBI <a href="https://github.com/r-dbi/DBI" target="_blank"><i class="fas fa-external-link-square-alt"></i></a></small>

```{r, eval=FALSE}
library(DBI)
library(odbc)  # Pilotes ODBC

# Dépend de la base de données
con <- DBI::dbConnect(odbc::odbc(),  # RSQLite::SQLite(), etc.
                      Driver     = "Nom du pilote",
                      servername = "Chemin vers serveur",
                      database   = "Nom",
                      UID        = rstudioapi::askForPassword(),
                      PWD        = rstudioapi::askForPassword(),
                      Port       = 123456)

# Requêtes ...

DBI::dbDisconnect(con)
```
## Requêtes {#query}

```{r, eval = FALSE}
dbListTables(con)
dbWriteTable(con, "tableau", tableau)
dbListFields(con, "tableau")

res <- dbSendQuery(con, "SELECT * FROM tableau WHERE colonne = 2020")
dbFetch(res, n = -1)
dbClearResult(res)

DBI::dbDisconnect(con)
```


