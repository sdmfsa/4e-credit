# Ateliers 4-e crédit

Ces ateliers sont suivis dans le cadre des cours GSF-6016, GSF-6022 et GSF-6029 à l'[Université Laval](https://www4.fsa.ulaval.ca/la-faculte/salles-des-marches).

Les ateliers interactifs sont créés avec le paquetage [learnr](https://rstudio.github.io/learnr/) et peuvent être utilisés localement, sur un [serveur shiny](https://rstudio.com/products/shiny/shiny-server/) ou via [ShinyProxy](https://www.shinyproxy.io/getting-started/). Une version web des ateliers est disponible [ici](http://sdmapps.ca/4e/).

## Utilisation via RStudio

Il suffit d'ouvrir le fichier `index.Rmd` avec RStudio et de cliquer sur le bouton `Run Document`.

## Utilisation via l'invite de commande

Vous devez d'abord créer le fichier `index.html` à partir de `index.Rmd` :

```bash
Rscript -e 'rmarkdown::render("index.Rmd")'
```

Ensuite, il suffit d'utiliser la fonction `run` du paquetage `rmarkdown` :

```bash
Rscript -e 'rmarkdown::run(shiny_args=list(port=5000))'
```

### Dépendances

Vous devez vous assurer d'avoir installé `pandoc` et `pandoc-citeproc`. Pour ce faire, utilisez votre système de gestion de paquetages, par exemple :

+ Debian/Ubuntu :

```bash
apt install pandoc pandoc-citeproc
```

+ MAC :

```bash
brew install pandoc pandoc-citeproc
```

## Shiny server

Si vous déployez les ateliers sur un serveur `shiny`, assurez-vous de redémarrer le service `shiny-server` après avoir fait des modifications. Par exemple, la commande `systemctl restart shiny-server` est utilisée pour redémarrer le serveur `shiny` sur des serveurs avec `systemd`. Pour plus de détails, consultez la [documentation](https://docs.rstudio.com/shiny-server/).

### Installation des paquetages

Vous devez vous assurer que les paquetages utilisés par le tutoriel sont accessibles para l'utilisateur `shiny` :

```bash
sudo su shiny
Rscript -e 'install.packages(c("package1", "package2", "etc"))'
```

Pour plus de détails, consultez la documentation [officielle](https://docs.rstudio.com/shiny-server/)

